# cl-ipfs
This is an unnoficial IPFS API client written in Common Lisp.

## Dependencies
-cl-drakma
-cl-yason

As a prerequisite, the IPFS daemon should obviously be running.

## Examples
You can use the system through asdf. Start a REPL and do the following:

	(require 'asdf)
	(asdf:load-system "cl-ipfs")

Some examples:

	(cl-ipfs:add "/home/alex/Desktop/emacs.png")
	(cl-ipfs:size "QmaGu6zzshKqhew4HiEv76JM7xdSJj7rLnyLxWszip47Mp")
	(cl-ipfs:ls "QmaGu6zzshKqhew4HiEv76JM7xdSJj7rLnyLxWszip47Mp") ;; would return nil, since the file doesn't have any links.
	(cl-ipfs:pinadd "QmaGu6zzshKqhew4HiEv76JM7xdSJj7rLnyLxWszip47Mp")
