(in-package #:cl-ipfs)

(defvar gatewayip "localhost")
(defvar gatewayport "8080")
(defvar apiport "5001")
;; (setf drakma:*header-stream* *standard-output*)

(defun ls(hash)
  (nth 3(caadr (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/ls/" hash) :jsonparsing t))))

(defun cat(hash)
  (http-req (concatenate 'string "http://" gatewayip ":" gatewayport "/ipfs/" hash)))

(defun size(hash)
  (last (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/object/stat/" hash) :jsonparsing t)))

(defun add(path)
  (nth 3 (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/add") :filepath path)))

(defun pinadd(hash)
  (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/pin/add/" hash) :jsonparsing t))

(defun pinrm(hash)
  (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/pin/rm/" hash) :jsonparsing t))

(defun id()
  (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/id") :jsonparsing t))

(defun version()
  (http-req (concatenate 'string "http://" gatewayip ":" apiport "/api/v0/version") :jsonparsing t))

(defun http-req(url &key (filepath nil) (jsonparsing nil))
  (cond ((not (eq filepath nil))
	 (let ((stream (drakma:http-request url
			:method :post
			:parameters  `(("file" . ,(pathname filepath)))
			:content-type "multipart/form-data"
			:want-stream t)))

	   (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)
	   (yason:parse stream :object-as :plist)))

	((eq jsonparsing t)
	 (let ((stream (drakma:http-request url :want-stream t)))
	   (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)
	   (yason:parse stream :object-as :plist)))

	((eq jsonparsing nil)
	 (drakma:http-request url))))
