# cl-ipfs

An IPFS API client written in Common Lisp as an asdf system.


## Functions
### (cl-ipfs:add "/path/to/file")
---

Input: Valid path for a file located on the current filesystem.

Output: an IPFS hash.

### (cl-ipfs:ls "hash") 
---

Input : an IPFS hash.

Output: IPFS hashes to the links (that is links to the other parts of the uploaded file.)


### (cl-ipfs:cat "hash")
---
Input : an IPFS hash.

Output: a raw byte-stream of the data.

Input : an IPFS hash.

### (cl-ipfs:size "hash")
---

Input : an IPFS hash.

Output: file size in bits.
