(cl:defpackage #:cl-ipfs
  (:documentation "IPFS API client. Requires drakma and yason. Full documentation available at http://www.gitlab.com/alexm98/cl-ipfs")
  (:use #:common-lisp #:asdf #:drakma #:yason)
  (:export #:ls #:cat #:size #:add #:pinadd #:pinrm
	   #:id #:version))
