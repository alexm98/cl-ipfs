(defsystem "cl-ipfs"
    :description "An IPFS API client."
    :author "Alex Munteanu <munteanu.c.alex@gmail.com>"
    :version "0.1"
    :license "GPLv3"
    :depends-on ("drakma" "yason")
    :serial t
    :components ((:file "packages")
		 (:file "cl-ipfs")))
